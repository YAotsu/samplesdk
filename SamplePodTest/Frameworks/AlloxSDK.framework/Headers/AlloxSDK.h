//
//  AlloxSDK.h
//  AlloxSDK
//
//  Created by 青津 祐太 on 2019/09/30.
//  Copyright © 2019 Aotsu Yuta. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AlloxSDK.
FOUNDATION_EXPORT double AlloxSDKVersionNumber;

//! Project version string for AlloxSDK.
FOUNDATION_EXPORT const unsigned char AlloxSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AlloxSDK/PublicHeader.h>

//#import "<AlloxSDK/AlloxAdManager.h>"
