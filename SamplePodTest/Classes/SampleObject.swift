
import Foundation

@objc public class SampleObject: NSObject {
    
    @objc public func hoge() -> String {
        return "hoge";
    }
}
