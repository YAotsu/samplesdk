#
# Be sure to run `pod lib lint SamplePodTest.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SamplePodTest'
  s.version          = '0.1.1'
  s.summary          = 'A short description of SamplePodTest.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/YAotsu/samplesdk'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = 'ddt003@d2c.co.jp'
  s.source           = { :git => 'https://YAotsu@bitbucket.org/YAotsu/samplesdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'SamplePodTest/Classes/**/*'
  s.ios.vendored_frameworks = 'SamplePodTest/Frameworks/AlloxSDK.framework'
  # s.framework = 'AlloxSDK'
  s.swift_version = '5.0'

  # s.resource_bundles = {
  #   'SamplePodTest' => ['SamplePodTest/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
